function counterFactory() {
  const factory = {
    increment: function () {
      counter += 1;
      return counter;
    },
    decrement: function () {
      counter--;
      return counter;
    },
  };
  return factory;
}
module.exports = { counterFactory };
