function cacheFunction(cb) {
  let cache = {};
  return function (...args) {
    //passing the arguments to the returned function.
    let key = args;
    if (typeof args == "object" || typeof args == "array") {
      //Edge case
      key = JSON.stringify(args);
    }
    if (cache.hasOwnProperty(key)) {
      //check if key already exists in the cache object.
      console.log(
        "The arguments already exists in the cache, fetching cache result..."
      );
      return cache[key];
    } else {
      const result = cb(...args);
      cache[key] = result;
      return result;
    }
  };
}
module.exports = { cacheFunction };
