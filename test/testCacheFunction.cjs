let cachedFunctionImport = require("../cachedFunction.cjs");
const callbackFunction = (x, y) => {
  return x + y;
};
// console.log(createCachedFunction)
const cachedOperation = cachedFunctionImport.cacheFunction(callbackFunction);
console.log(cachedOperation(2, 3)); // Invokes expensiveOperation(2, 3) and caches the result
console.log(cachedOperation(2, 3)); // Uses the cached result
console.log(cachedOperation(4, 5)); // Invokes expensiveOperation(4, 5) and caches the result
console.log(cachedOperation(2, 3)); // Uses the cached result again
