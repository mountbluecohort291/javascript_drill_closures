const limitFunctionCallCount = require("../limitFunctionCallCount");

function test(counter) {
  console.log(`called ${counter} times.`);
}

const limitedFunction = limitFunctionCallCount(test, 4);

limitedFunction(); // Call the returned function multiple times
limitedFunction();
limitedFunction();
limitedFunction();
limitedFunction(); // This will reach the limit and log a warning
