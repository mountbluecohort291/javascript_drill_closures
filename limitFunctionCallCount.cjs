function limitFunctionCallCount(cb, n) {
  let Count = 0;
  return function () {
    if (Count < n) {
      Count++;
      cb(Count);
    } else {
      console.warn("Function call limit reached:", n);
      return null;
    }
  };
}

module.exports = limitFunctionCallCount;
